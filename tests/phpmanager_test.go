package tests

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"testing"
)

func TestStartPhpManager(t *testing.T) {
	l, err := net.ListenTCP("tcp", &net.TCPAddr{
		IP:   net.ParseIP("0.0.0.0"),
		Port: 9000,
	})
	if err != nil {
		panic(err)
	}
	//f, _ := os.OpenFile("text.out", os.O_CREATE|os.O_RDWR, os.ModePerm)
	//go Processes(f)

	for {
		conn, err := l.AcceptTCP()
		if err != nil {
			panic(err)
		}

		go func(co *net.TCPConn) {
			defer co.Close()

			client, err := net.DialTCP("tcp", nil, &net.TCPAddr{
				IP: net.ParseIP("127.0.0.1"),
				Port: 3005,
			})
			defer client.Close()
			if err != nil {
				fmt.Println("connection error: ", err.Error())
				return
			}

			go func() {
				for {
					buf := make([]byte, 10240)
					n, err := co.Read(buf)
					if err != nil {
						break
					}
					client.Write(buf[:n])
				}
			}()

			for {
				buf := make([]byte, 10240)
				n, err := client.Read(buf)
				if err != nil {
					break
				}
				co.Write(buf[:n])
			}
		}(conn)

	}

}

func Processes(in *os.File) bool {
	var error error
	env := os.Environ()
	attr := &os.ProcAttr{
		Dir: "E:\\vala\\php-7.3",
		Files: []*os.File{
			in,
			in,
			os.Stderr,
		},
		Env: env,
	}
	////E:\vala\php-7.3\php-cgi.exe -c php.ini -b 0.0.0.0:9000
	process, error := os.StartProcess("E:\\vala\\php-7.3\\php-cgi.exe", []string{"php-cgi.exe", "-c", "php.ini", "-b", "0.0.0.0:3005"}, attr)
	//phpService.Process, error = os.StartProcess(phpService.ExecFile, []string{"php-cgi-spawner.exe", "'php-7.3/php-cgi.exe -c php-7.3/php.ini'", "9000", "4+16"}, attr)
	if error != nil {
		panic(error)
	}

	processState, error := process.Wait()
	if error != nil {
		panic(error)
	}

	if processState.Success() {
		return true
	}

	return false
}

func TestServer(t *testing.T)  {
	server()
}

func server() {
	lis, err := net.Listen("tcp", ":9000")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer lis.Close()
	for {
		conn, err := lis.Accept()
		if err != nil {
			fmt.Printf("建立连接错误:%v \n", err)
			continue
		}
		fmt.Println(conn.RemoteAddr(), conn.LocalAddr())
		go handle(conn)
	}
}
func handle(sconn net.Conn) {
	defer func() {
		log.Println("WANJIE")
		sconn.Close()
	}()
	dconn, err := net.Dial("tcp", ":3005")
	if err != nil {
		fmt.Printf("连接%v失败:%v\n", "3005", err)
		return
	}
	ExitChan := make(chan bool, 1)
	go func(sconn net.Conn, dconn net.Conn, Exit chan bool) {
		_, err := io.Copy(dconn, sconn)
		fmt.Printf("往%v发送数据失败:%v\n", "3005", err)
		Exit <- true
	}(sconn, dconn, ExitChan)
	go func(sconn net.Conn, dconn net.Conn, Exit chan bool) {
		_, err := io.Copy(sconn, dconn)
		fmt.Printf("从%v接收数据失败:%v\n", "3005", err)
		Exit <- true
	}(sconn, dconn, ExitChan)
	<-ExitChan
	dconn.Close()
}

func TestPhpServer(t *testing.T)  {

}
