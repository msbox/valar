package tests

import (
	"encoding/gob"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"vala/repositorys"
	"vala/tools"
)

type Config struct {
	Name string `yaml:name`
	Cmd string `yaml:cmd`
	PidFile string 	`yaml:pidFile`
	Args []string `yaml:args`
}

func TestYaml(t *testing.T)  {
	var setting Config
	config, err := ioutil.ReadFile("./test.yaml")
	if err != nil {
		fmt.Print(err)
	}
	yaml.Unmarshal(config,&setting)
	if !filepath.IsAbs(setting.Args[1]) {
		setting.Args[1] = filepath.Join(tools.GetBasePath(), setting.Args[1])
	}

	fmt.Println(setting)
}

func TestEnCode(t *testing.T)  {
	file, _ := os.OpenFile("data.gob", os.O_CREATE|os.O_RDWR, os.ModePerm)
	gob.NewEncoder(file).Encode([]string{"1","2","4"})
}

func TestDeCode(t *testing.T)  {
	var arr []string
	file, _ := os.OpenFile("data.gob", os.O_RDWR, os.ModePerm)
	gob.NewDecoder(file).Decode(&arr)

	fmt.Println(arr)
}

func TestCreate(t *testing.T)  {
	site := repositorys.Site{
		Name: "service1",
		Path: "/AC",
	}
	repositorys.Add(site)
}

func TestQuery(t *testing.T)  {
	fmt.Printf("%v", repositorys.Query())
}

func TestDelete(t *testing.T)  {
	repositorys.Delete(repositorys.Site{
		Name: "service",
	})
}