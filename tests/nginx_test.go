package tests

import (
	"fmt"
	"github.com/winlabs/gowin32"
	"os"
	"path/filepath"
	"testing"
	"vala/services"
)

func TestPath(t *testing.T)  {
	fmt.Println(filepath.Dir("C:\\Users\\hui.zhou\\Desktop\\vala\\nginx\\nginx.exe"))
}

// 开始
func TestNginxStart(t *testing.T)  {
	service := services.NewNginxService("E:\\vala\\nginx\\nginx.exe")
	service.Start()
	fmt.Println(123)
}

// 停止
func TestNginxStop(t *testing.T)  {
	service := services.NewNginxService("E:\\vala\\nginx\\nginx.exe")
	service.Stop()
}


func TestCreateNginxService(t *testing.T)  {
	scManager,_ := gowin32.OpenLocalSCManager()
	service,err:=scManager.CreateService("NginxService", &gowin32.ServiceConfig{
		ServiceType: gowin32.ServiceWin32OwnProcess,
		StartType: gowin32.ServiceAutoStart,
		ErrorControl: gowin32.ServiceErrorNormal,
		//BinaryPathName:"E:\\vala\\php-cgi-spawner.exe \"php-7.3/php-cgi.exe -c php-7.3/php.ini\" 9000 0+16",
		BinaryPathName:"C:\\Users\\hui.zhou\\Desktop\\vala\\srvany.exe",
		ServiceStartName: "NginxService",
		DisplayName:"NginxService",
	},gowin32.ServiceConfigStartType|gowin32.ServiceConfigBinaryPathName|gowin32.ServiceConfigDisplayName)
	if err!=nil {
		panic(err)
	}

	fmt.Println(service.GetStatus())
}

func TestStartNginxService(t *testing.T)  {
	scManager,_ := gowin32.OpenLocalSCManager()
	service,_:=scManager.OpenService("NginxService")
	//E:\vala\nginx\nginx.exe -c E:\vala\nginx\nginx\conf\nginx.conf
	err:=service.Start([]string{
		"E:\\vala\\nginx\\nginx.exe",
		//"-c ",
		//"E:\\vala\\nginx\\nginx\\conf\\nginx.conf",
	})
	if err!=nil {
		panic(err)
	}

	fmt.Println(service.GetStatus())
}

func TestStartNginxProcess(t *testing.T)  {
	env := os.Environ()
	attr := &os.ProcAttr{
		Dir: "E:\\vala\\nginx",
		Files: []*os.File{
			os.Stdin,
			os.Stdout,
			os.Stderr,
		},
		Env: env,
	}
	process,error := os.StartProcess("E:\\vala\\nginx\\nginx.exe",[]string{"nginx.exe"}, attr)
	if error != nil {
		panic(error)
	}

	processState,error := process.Wait()
	if error !=nil {
		panic(error)
	}

	if processState.Success() {
		fmt.Printf("PID : %d", processState.Pid())
	}
}
