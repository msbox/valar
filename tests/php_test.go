package tests

import (
	"fmt"
	"github.com/winlabs/gowin32"
	"os"
	"path/filepath"
	"testing"
	"vala/services"
	"vala/tools"
)

func TestCreateService(t *testing.T) {
	scManager, _ := gowin32.OpenLocalSCManager()
	service, err := scManager.CreateService("PHPService", &gowin32.ServiceConfig{
		ServiceType:  gowin32.ServiceWin32OwnProcess,
		StartType:    gowin32.ServiceAutoStart,
		ErrorControl: gowin32.ServiceErrorNormal,
		//BinaryPathName:"E:\\vala\\php-cgi-spawner.exe \"php-7.3/php-cgi.exe -c php-7.3/php.ini\" 9000 0+16",
		BinaryPathName:   "C:\\Users\\hui.zhou\\Desktop\\vala\\srvany.exe",
		ServiceStartName: "PHPService",
		DisplayName:      "PHPService",
	}, gowin32.ServiceConfigStartType|gowin32.ServiceConfigBinaryPathName|gowin32.ServiceConfigDisplayName)
	if err != nil {
		panic(err)
	}

	fmt.Println(service.GetStatus())
}

// 开始
func TestStartService(t *testing.T) {
	//homePath := os.Getenv("VALA_HOME")
	//fmt.Println(homePath+"\\php-7.3\\php-cgi.exe");
	//fmt.Println(homePath+"\\srvany.exe");

	service := services.NewPHPService("E:\\vala\\php-7.3\\php-cgi.exe")
	service.Start()
}

// 停止
func TestStopPhpService(t *testing.T) {
	service := services.NewPHPService("E:\\vala\\php-7.3\\php-cgi.exe")
	service.Stop()
}

func TestAbsPath(t *testing.T) {
	fmt.Println(tools.GetAbsPath("."))
	fmt.Println(filepath.Join(tools.GetAbsPath("."), "nginx.conf"))
}

func TestStartPHPProcess(t *testing.T) {
	fmt.Printf("%d",os.Getppid());
	env := os.Environ()
	attr := &os.ProcAttr{
		Dir: "E:\\vala\\php-7.3",
		Files: []*os.File{
			os.Stdin,
			os.Stdout,
			os.Stderr,
		},
		Env: env,
	}
	////E:\vala\php-7.3\php-cgi.exe -c php.ini -b 0.0.0.0:9000
	process, error := os.StartProcess("E:\\vala\\php-7.3\\php-cgi.exe", []string{"php-cgi.exe", "-c", "php.ini", "-b", "0.0.0.0:9000"}, attr)
	if error != nil {
		panic(error)
	}

	processState, error := process.Wait()
	if error != nil {
		panic(error)
	}

	if processState.Success() {
		fmt.Printf("PID : %d", processState.Pid())
	}

	fmt.Printf("QUIT")
}

func TestStartPhp(t *testing.T)  {

}