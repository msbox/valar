package tests

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"path/filepath"
	"testing"
	"vala/clients"
	"vala/servers"
	"vala/services"
	"vala/tools"
)

func TestStartServer(t *testing.T)  {
	server := &servers.Server{
		Host: "0.0.0.0",
		Port: "3002",
	}

	server.Handle()

	server.Run()

}

func TestClient(t *testing.T)  {
	resp, _ := http.PostForm("http://127.0.0.1:3002/services",
		url.Values{"action": []string{"start"}, "service": []string{"php"}})
	fmt.Println(resp.StatusCode)
	byte,_ := ioutil.ReadAll(resp.Body)
	fmt.Printf(tools.FormatJson(byte))
}

func TestClientStop(t *testing.T)  {
	resp, _ := http.PostForm("http://127.0.0.1:3002/services",
		url.Values{"action": []string{"stop"}, "service": []string{"php"}})
	fmt.Println(resp.StatusCode)
	byte,_ := ioutil.ReadAll(resp.Body)
	fmt.Printf(tools.FormatJson(byte))
}

func TestClientLs(t *testing.T)  {
	resp, _ := http.PostForm("http://127.0.0.1:3002/services",
		url.Values{"action": []string{"ls"}, "service": []string{"nginx"}})
	fmt.Println(resp.StatusCode)
	byte,_ := ioutil.ReadAll(resp.Body)
	fmt.Printf(tools.FormatJson(byte))
}

func TestClientAddSite(t *testing.T)  {
	client := clients.Client{
		Host: "127.0.0.1",
		Port: "3002",
	}
	client.Sites("create", "service", "E:\\web\\opencart")
}

func TestClientListSite(t *testing.T)  {
	client := clients.Client{
		Host: "127.0.0.1",
		Port: "3002",
	}
	client.Sites("ls", "service", "E:\\web\\opencart")
}

func TestClientDeleteSite(t *testing.T)  {
	client := clients.Client{
		Host: "127.0.0.1",
		Port: "3002",
	}
	client.Sites("delete", "service1", "E:\\web\\opencart")
}

func TestClientUpdateSite(t *testing.T)  {
	client := clients.Client{
		Host: "127.0.0.1",
		Port: "3002",
	}
	client.Sites("update", "service", "E:\\web\\service")
}

func TestWriteHosts(t *testing.T)  {
	tools.AppendHosts("test123.com")
}

func TestDeleteHosts(t *testing.T)  {
	tools.DeleteHosts("test.com")
}

func TestUpdateHosts(t *testing.T)  {
	tools.UpdateHosts("test1.com", "testa2.com")
}

func TestLoadDir(t *testing.T)  {
	files, error := ioutil.ReadDir(filepath.Join(tools.GetBasePath(), "Extensions"))
	if error != nil {
		panic(error)
	}
	var serviceDefines []services.ServiceDefine
	for _, file := range files{
		if file.IsDir() {
			serviceDefines = append(serviceDefines, services.ServiceDefine{
				Name: file.Name(),
				Dir: file.Name(),
			})
		}
	}
	fmt.Println(serviceDefines)
}

func TestMysql(t *testing.T)  {
	m := services.NewServiceManager()
	s,_:=m.GetService("mysql5.7")
	fmt.Println(s.Status())
}