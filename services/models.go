package services

type Service interface {
	Start()
	Stop()
	Reload()
	Status() bool
}

type ServiceInfo struct {
	Name string
	Flag string
	Status bool
}

type ServiceConfig struct {
	Name string `yaml:name`
	Cmd string `yaml:cmd`
	PidFile string 	`yaml:pidFile`
	Args []string `yaml:args`
}