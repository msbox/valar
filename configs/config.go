package configs

const DATA_PATH = "data.gob"
const CONFIG_DIR = "config.d"
const NGINX_DIR = "nginx"
const PHP_DIR = "php-7.3"
const NGINX_PID_DIR = "logs"
const NGINX_PID_FILE = "nginx.pid"
const NGINX_CONFIG_EXT = ".conf"