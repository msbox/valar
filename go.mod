module vala

go 1.15

require (
	github.com/winlabs/gowin32 v0.0.0-20201021184923-54ddf04f16e6
	gopkg.in/yaml.v2 v2.4.0
)
