# Valar

#### 介绍
基于go语言开发，轻松管理 Windows环境下php本地开发环境。

#### 软件架构
整体架构图
![输入图片说明](https://images.gitee.com/uploads/images/2020/1206/190546_e43367c2_782399.png "整体结构图.png")

#### 安装教程

1.  下载Valar软件包直接解压即可 [百度网盘下载链接](https://pan.baidu.com/s/1H7xdUdKfGB9x8m8gu0LdsQ) 提取码: vhq7 
2.  将Valar目录加入到WIndows环境变量的PATH中
3.  运行命令创建windows服务并启动服务 （需要管理员权限否则会提示权限不够）

```
# 创建服务
valar.exe server create

# 启动服务
valar.exe server start

# 查看服务状态
valar.exe server status
```
4. 浏览器访问 http://localhost:3002 进入管理面板


#### 使用说明

1.  web管理面板管理
![输入图片说明](https://images.gitee.com/uploads/images/2020/1206/191246_b317fd1f_782399.png "valar-web.png")
2.  命令行管理

```
$ valar.exe -h

Vala Command Document
  Vala version: vala/1.0.0
  Usage: vala [service|site] [command]
  Options:
   server               : server service manager
   service              : service manager command
   site                 : site manager command"

  Command:
   server
    status              : query server status
    start               : boot server
    stop                : stop server
   service
    ls                  : query all service status
    start               : [nginx|php] boot service
    stop                : [nginx|php] stop service
    reload              : [nginx|php] reload service
   site
    ls                  : query all site status
    create              : create site to vala
    update              : update site info to vala
    delete              : delte site from vala

```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
