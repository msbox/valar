package main

import (
	"log"
	"os"
	"vala/servers"
)

func main() {
	args := os.Args
	if len(args) < 3 {
		log.Fatalf("Input Params Error!")
	}
	server := &servers.Server{
		Host: args[1],
		Port: args[2],
	}

	//c := make(chan os.Signal, 1)
	//signal.Notify(c, os.Interrupt, os.Kill)

	// 处理器
	server.Handle()

	// 运行服务
	server.Run()

	//s := <-c
	//server.Release()
	//fmt.Println("Quit Signal:", s)
}
