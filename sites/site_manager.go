package sites

import (
	"os"
	"path/filepath"
	"vala/configs"
	"vala/repositorys"
)

type SiteManager struct {
	SiteConfigPath string
}

func NewSiteManager(path string) SiteManager {
	return SiteManager{
		SiteConfigPath: path,
	}
}

func (siteManager SiteManager) List() []repositorys.Site {
	return repositorys.Query()
}

func (siteManager SiteManager) Add(site repositorys.Site) {
	// 如果有模板则直接写入
	file, error := os.OpenFile(filepath.Join(siteManager.SiteConfigPath, site.Name+configs.NGINX_CONFIG_EXT), os.O_WRONLY|os.O_CREATE, 0644)
	if error != nil {
		panic(error)
	}
	file.WriteString(site.Template.ConsTemplateContent())
	defer file.Close()

	repositorys.Add(site)
	// 注册域名映射
	//tools.AppendHosts(site.Template.ServerName)
}

func (siteManager SiteManager) Exists(site repositorys.Site) bool {
	_, error := repositorys.Find(site)
	if error != nil {
		return false
	} else {
		return true
	}
}

func (siteManager SiteManager) Delete(site repositorys.Site) {
	site, error := repositorys.Find(site)
	if error != nil {
		panic(error)
	}
	os.Remove(filepath.Join(siteManager.SiteConfigPath, site.Name+configs.NGINX_CONFIG_EXT))
	repositorys.Delete(site)
	// 删除域名映射
	//tools.DeleteHosts(site.Template.ServerName)
}

func (siteManager SiteManager) Update(site repositorys.Site) {
	siteManager.Delete(site)
	siteManager.Add(site)
}
