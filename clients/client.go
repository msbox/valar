package clients

import (
	"fmt"
	"github.com/winlabs/gowin32"
	"io/ioutil"
	"net/http"
	"net/url"
	"path/filepath"
	"vala/tools"
)

type Client struct {
	Host string
	Port string
}

func (client Client) Command(command string, action string, service string) {
	resp, _ := http.PostForm("http://"+client.Host+":"+client.Port+"/"+command,
		url.Values{"action": []string{action}, "service": []string{service}})
	// 输出信息
	bts, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode == http.StatusOK {
		fmt.Printf("%s", tools.FormatJson(bts))
	} else {
		fmt.Printf("Error Code %d \n", resp.StatusCode)
		fmt.Println(tools.FormatJson(bts))
	}
}

// 服务管理
func (client Client) Services(action string, service string) {
	client.Command("services", action, service)
}

// 站点管理
func (client Client) Sites(action string, name string, path string) () {
	resp, _ := http.PostForm("http://"+client.Host+":"+client.Port+"/sites",
		url.Values{"action": []string{action}, "name": []string{name}, "path": []string{path}})
	// 输出信息
	bts, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode == http.StatusOK {
		fmt.Printf("%s", tools.FormatJson(bts))
	} else {
		fmt.Printf("Error Code %d \n", resp.StatusCode)
		fmt.Println(tools.FormatJson(bts))
	}
}

// 创建Server服务
func (client Client) CreateServerService() {
	scManager, _ := gowin32.OpenLocalSCManager()
	service, err := scManager.CreateService("ValaService", &gowin32.ServiceConfig{
		ServiceType:      gowin32.ServiceWin32OwnProcess,
		StartType:        gowin32.ServiceAutoStart,
		ErrorControl:     gowin32.ServiceErrorNormal,
		BinaryPathName:   filepath.Join(tools.GetBasePath(), "srvany.exe"),
		ServiceStartName: "ValaService",
		DisplayName:      "ValaService",
	}, gowin32.ServiceConfigStartType|gowin32.ServiceConfigBinaryPathName|gowin32.ServiceConfigDisplayName)
	if err != nil {
		panic(err)
	}

	statusInfo, _ := service.GetStatus()
	if statusInfo.CurrentState != gowin32.ServiceStopped {
		panic("status error")
	}
}

// 开启Server服务
func (client Client) StartServerService() {
	scManager, err := gowin32.OpenLocalSCManager()
	if err != nil {
		panic(err)
	}
	fmt.Println(filepath.Join(tools.GetBasePath(), "server.exe"))
	service, _ := scManager.OpenService("ValaService")
	err = service.Start([]string{
		filepath.Join(tools.GetBasePath(), "server.exe"),
		"0.0.0.0",
		"3002",
	})
	if err != nil {
		panic(err)
	}
}

// 停止Server服务
func (client Client) StopServerService() {
	scManager, err := gowin32.OpenLocalSCManager()
	if err != nil {
		panic(err)
	}
	service, _ := scManager.OpenService("ValaService")
	statusInfo, _ := service.GetStatus()
	if statusInfo.CurrentState == gowin32.ServiceRunning {
		_, err = service.Control(gowin32.ServiceControlStop)
		if err != nil {
			panic(err)
		}
	}
}

// 状态
func (client Client) StatusServerService() bool {
	scManager, err := gowin32.OpenLocalSCManager()
	if err != nil {
		panic(err)
	}
	service, _ := scManager.OpenService("ValaService")
	statusInfo, _ := service.GetStatus()

	return statusInfo.CurrentState == gowin32.ServiceRunning
}
