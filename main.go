package main

import (
	"fmt"
	"os"
	"path/filepath"
	"vala/clients"
	"vala/tools"
)

func help() {
	fmt.Printf(`
Vala Command Document 
  Vala version: vala/1.0.0
  Usage: vala [service|site] [command] 
  Options:
   server		: server service manager
   service		: service manager command
   site			: site manager command"

  Command:
   server 
    status		: query server status
    start		: boot server
    stop		: stop server
   service
    ls   		: query all service status
    start		: [nginx|php] boot service
    stop		: [nginx|php] stop service
    reload		: [nginx|php] reload service
   site
    ls    		: query all site status
    create		: create site to vala
    update		: update site info to vala
    delete		: delte site from vala
`	)
}

func main() {
	client := clients.Client{
		Host: "127.0.0.1",
		Port: "3002",
	}
	// 输入参数
	args := os.Args
	if len(args) < 3 || args == nil {
		help()
		return
	}
	operate := args[1]
	action := args[2]
	switch operate {
	case "-h":
		help()
	case "service":
		// service [ls|start|stop|reload] [service_name]
		if len(args) > 3 {
			client.Services(action, args[3])
		} else {
			client.Services(action, "")
		}
	case "site":
		// site [create|update|delete|ls] [name] [path]
		if len(args) > 4 {
			path, error := filepath.Abs(args[4])
			if error != nil {
				panic(error)
			}
			client.Sites(action, args[3], path)
		} else if len(args) > 3 {
			client.Sites(action, args[3], "")
		} else {
			client.Sites(action, "", "")
		}

	case "server":
		switch action {
		case "create":
			client.CreateServerService()
			fmt.Printf("Status: %s \n", "Success")
		case "start":
			client.StartServerService()
			fmt.Printf("Status: %s \n", "Success")
		case "stop":
			client.StopServerService()
			fmt.Printf("Status: %s \n", "Success")
		case "status":
			fmt.Printf("Status: %s \n", tools.FormatEnabled(client.StatusServerService()))
		}
	}

}
